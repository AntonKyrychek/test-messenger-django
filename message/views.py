from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView
from .models import Message
from .permissions import IsOwnerOrReadOnly, IsAuthenticated
from .serializers import MessageSerializer
from .pagination import CustomPagination
from django.contrib.auth.models import User


class GetDeleteUpdateMsgs(RetrieveUpdateDestroyAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)

    def get_queryset(self, id):
        try:
            return Message.objects.get(pk=id)
        except Message.DoesNotExist:
            content = {
                'status': 'Not Found'
            }
            return Response(content, status=status.HTTP_404_NOT_FOUND)

    # Get a Message
    def get(self, request, id):

        msg = self.get_queryset(id)
        serializer = MessageSerializer(msg)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Update a Message
    def put(self, request, id):
        
        message = self.get_queryset(id)

        if request.user == message.creator:
            serializer = MessageSerializer(message, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)

    # Delete a Message
    def delete(self, request, id):

        message = self.get_queryset(id)
        if request.user == message.creator or message.receiver == request.user.username:
            message.delete()
            content = {
                'status': 'OK'
            }
            return Response(content, status=status.HTTP_200_OK)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)
   

class GetPostMsgs(ListCreateAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination
    
    def get_queryset(self):
       return Message.objects.all()

    # Get all message
    def get(self, request):
        messages = self.get_queryset()
        paginate_queryset = self.paginate_queryset(messages)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    # Create a new message
    def post(self, request):
        serializer = MessageSerializer(data=request.data)
        try:
            User.objects.get(username=request.data["receiver"])
        except:
            raise Exception("no such user")
        # if request.data["receiver"] == request.user.username:
        #     raise Exception("Cannot send msgs to yourself")
        if serializer.is_valid():
            serializer.save(creator=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetInbox(ListCreateAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_queryset(self, request):
        username = request.user.username
        return Message.objects.filter(receiver=username).all()

    # Get all message
    def get(self, request):
        messages = self.get_queryset(request)
        paginate_queryset = self.paginate_queryset(messages)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class GetUnreadInbox(ListCreateAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = CustomPagination

    def get_queryset(self, request):
        username = request.user.username
        return Message.objects.filter(receiver=username, read=False).all()

    # Get all message
    def get(self, request):
        messages = self.get_queryset(request)
        paginate_queryset = self.paginate_queryset(messages)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class GetOneInbox(RetrieveUpdateDestroyAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self, id):
        try:
            return Message.objects.get(pk=id)
        except Message.DoesNotExist:
            raise


    # Get a inbox msg
    def get(self, request, id):
        msg = self.get_queryset(id)
        if not isinstance(msg, Message.DoesNotExist):
            if not msg.receiver == request.user.username:
                raise Exception("Not your Messsage to read")
            msg.read = True
            msg.save()
        serializer = MessageSerializer(msg)
        return Response(serializer.data, status=status.HTTP_200_OK)
