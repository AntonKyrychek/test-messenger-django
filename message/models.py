from django.db import models


class Message(models.Model):
    subject = models.CharField(max_length=100)
    message = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey('auth.User', related_name='created_messages', on_delete=models.CASCADE)
    receiver = models.CharField(max_length=20)
    read = models.BooleanField(default=False)
