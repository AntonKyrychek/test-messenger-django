from django.urls import  path, re_path
from . import views


urlpatterns = [
    re_path(r'^api/v1/message/(?P<id>[0-9]+)$',  # Url to get, update or delete a message
            views.GetDeleteUpdateMsgs.as_view(),
            name='get_delete_update_message'
            ),
    path('api/v1/message/',  # urls list all or create new message
         views.GetPostMsgs.as_view(),
         name='get_post_message'
        ),
    path('api/v1/inbox/',  # urls list all inbox messages
         views.GetInbox.as_view(),
         name='get_inbox'
         ),
    re_path(r'^api/v1/inbox/(?P<id>[0-9]+)$',  # Url to get an inbox message
            views.GetOneInbox.as_view(),
            name='get_one_inbox'
        ),
    path('api/v1/unread_inbox/',  # urls list all inbox messages
         views.GetUnreadInbox.as_view(),
         name='get_unread_inbox'
         )
]
