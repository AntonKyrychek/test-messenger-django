from rest_framework import serializers
from .models import Message
from django.contrib.auth.models import User


class MessageSerializer(serializers.ModelSerializer):
    creator = serializers.ReadOnlyField(source='creator.username')

    class Meta:
        model = Message
        fields = ('id', 'subject', 'message', 'created_at', 'updated_at', 'creator', 'receiver', 'read')


class UserSerializer(serializers.ModelSerializer):
    inbox = serializers.PrimaryKeyRelatedField(many=True, queryset=Message.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'created_messages')
